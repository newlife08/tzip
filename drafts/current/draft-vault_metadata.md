---
tzip: 0xx
title: Rich Vault Metadata
author: Keefer Taylor (keefer@hover.engineering)
status: Draft
type: Interface
created: 2021-29-22
---

## Abstract

This proposal is an extension of [TZIP-016][1] and describes a metadata schema
and standards for Vault contracts.

Vault contracts are a class of contract which holds user's funds in escrow for use with
a dapp. 

## Motivation & Goals

Vault contracts are generally tied to a particular address as a consequence of interacting with a dApp. When a user
interacts with a dApp, they first create a vault, then they interact with the vault to interact with the dapp. 

Some examples of vault contracts may be:
- A collateralized debt position (CDP) where loans of a synthetic asset are taken against collateral
- A smart contract which locks an L1 asset and issues a tokenized L1 asset which may be redeemed
- A smart contract which is permanently delegated to a baker, and mints the user tokens to track their delegation

Generally, a dApp will create many contracts that represent one or more vaults for a user. These
contracts are otherwise indistinguisable from other business logic smart contracts, but logically they are strongly 
attached to an implicit account. 

This strong association and duplication makes vaults a good candidate for integration in a number
of tools, including wallet UIs, indexers and composition in other dApps. Since each dApp's vault
contract will have different entry points, storage mechanisms and behavior, a common interface
for interacting with them would greatly simplify integrations. This TZIP is motivated by simplifying that process. 

This metadata standard aims to:

1. Create a widely recognized and easy to understand metadata standard for vault contracts
2. Provide both extensibility and versioning for the standard, such that this standard can adapt to emerging technologies
3. Conform to existing and emerging standards
4. Provide interoperability among ecosystem members (contracts, indexers, wallets, libraries, etc)


## Standards and Recommendations

_All fields are defined and described in the Schema Definition section of the document._

It is strongly advised -- but not required -- that all vaults follow the following
standards and reccomendations.


## Metadata Representation

Vault metadata is intended for off-chain, user-facing contexts (e.g.  wallets,
explorers, marketplaces).

### Vault-Metadata Values
Token-specific metadata is stored/presented as a Michelson value of type
`(map string bytes)`. Reserved field names are defined in the 
[Vault Metadata Schema Fields section](#vault-metadata-schema-fields).

### Mutability

This standard does not make a recommendation on mutability of vault metadata, and hence all values should be assumed
to be mutable. 

### Off Chain Representations

Vaults metadata may be provided off chain by using a TZIP-016 URI which points to a JSON
representation of the vault metadata.

### On Chain Representation

Vault metadata can be represented on chain in two ways:

#### On Chain Big Map

Vault metadata is stored in a big-map annotated `%vault_metadata` of type `(big_map string bytes)`.

#### Off Chain View

Vault metadata is provided via an off chain view called `vault_metadata` and which returns type `(map string bytes)`.

### Precedence

Vaults should only choosed to have one representation of their metadata. However, if multiple representations are found, 
tooling should prefer the following precedence order:

1. Off Chain Representation
2. Off Chain View
3. On Chain Big Map

## Vault Metadata Schema Fields

### `` (string) - Optional

The empty string should correspond to a TZIP-016 URI which points to a JSON
representation of the vault metadata.

This field is optional and only used when metadata is represented off chain.

### `application` (string) - Required

The name of the application which originated the vault.

This field is required.

### `operators` (array) - Optional

A list of zero or more `operators` who are smart contrats or implicit accounts that may manage the vault on behalf of the
`owner`. 

This field is optional and may be ommitted if the vault does not have the concept of operators.

### `owner` (string) - Required

The `owner` of the vault. The `owner` is generally the smart contract or implicit account which originated the vault, and
which has global permissions.

This field is required.

### `version` (stringified number) - Required

The version of vault metadata being used. At current time, this version must always be `1`. 

This field is required.

## Examples

TODO(keefertaylor): Formulate and provide examples along with this TZIP when there is broader consensus. For illustrative 
purposes, an example could look like:

```
{
  "application": "Kolibri",
  "operators": ["tz1LcuQHNVQEWP2fZjk1QYZGNrfLDwrT3SyZ", "tz1irJKkXS2DBWkU1NnmFQx1c1L7pbGg4yhk"],
  "owner": "tz1abmz7jiCV2GH2u81LRrGgAFFgvQgiDiaf",
  "version": "1",
}
```

## Implementations

TODO(keefertaylor): Create and open source an implementation.

## Copyright

Copyright and related rights waived via
[CC0][2].

[1]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md
[2]: https://www.ietf.org/rfc/rfc2413.txt


