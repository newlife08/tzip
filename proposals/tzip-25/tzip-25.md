---
tzip: 025
title: QR Data Transmission Standard
author: Andreas Gassmann <a.gassmann@papers.ch>, Mike Godenzi <m.godenzi@papers.ch>
discussions-to: TBD
status: Draft
type: LA
created: 2021-11-30
---

## Summary

A protocol for exchanging data between offline signers and online applications through QR Codes.

## Abstract

This document describes the QR code transmission standard between offline signers and watch-only wallets.

## Motivation

Hardware Wallets have been the wallets of choice for secure cryptocurrency storage in the last few years. In this setup, the private key is stored on a secure device that is fully offline. They then connect to an internet connected device in order to receive transactions for signing. This communication often happens over USB cable or Bluetooth. While this offers good usability and compatibility, it is not ideal for security. QR Codes and file transfers have a few advantages:

- One Way Communication: Compared to USB or Bluetooth, QR Codes and file transfers are one way communication channels, so there is no way to send data back and forth without the users knowledge.
- Transparency and Security: Users can easily inspect the data in QR Codes. This allows users to see exactly what is being transferred between the two devices, increasing security.
- Better Compatibility: The QR Code data transmission can have better compatibility in some circumstances. USB compatibility is sometimes affected by software updates (eg. Browser update) and depends on the Operating System.
- Better User Experience: The QR Code data transmission can provide a better user experience compared to USB, especially in a mobile environment.
- Smaller Attack Surface. USB and Bluetooth have a higher attack surface than QR-Codes because it's a 2-way communication.

Because of these advantages, the QR Code and file data transmission is a better choice in some circumstances. Currently, there is no standard for how offline signers communicate with watch-only wallets and how the data has to be encoded. This TZIP presents a standard process and data transmission protocol for offline signers to work with watch-only wallets.

## Specification

### Communication Protocol Version 1.0.0 (Version Code 1)

Offline signer: The offline signer is a device or application which holds the users private keys and does not have network access.

Watch-only wallet: The watch-only wallet is the wallet which has network access and will interact with the blockchain, but doesn't hold any secrets.

### Process

In order to work with offline signers, the watch-only wallet should follow the following process:

1. Offline signer provides public key information to watch-only wallet via QR Code.
2. The watch-only wallet prepares the unsigned operation and sends it to the offline signer to sign via QR Code.
3. The offline signer signs the data and provides a signature back to the watch-only wallet via QR Code.
4. The watch-only wallet gets the signature and broadcasts the operation to the network.

### Versioning

All of the discussed messages will include a version number.

### Data transmission protocol

Since one QR Code can only contain a limited amount of data, animated QR Codes should be used for transmission when necessary. The BC-UR standard defined by BlockchainCommons describes a way to efficiently encode bytes in a sequence of QR codes. This TZIP will extend the BC-UR standard and add Tezos specific messages. All the data is encoded with CBOR. CDDL is the data definition language for CBOR. For more info about BC-UR, please check out the (specs)[https://github.com/BlockchainCommons/Research/blob/master/papers/bcr-2020-005-ur.md].

### Transport Layers

The main focus of this TZIP is the QR based communication. However, message structure that is defined here can also be used for a file based transport.

### Messages

#### Account Share

> Share an account from offline device to online device

> TODO CDDL representation

```ts
// Draft

export enum PublicKeyType {
  ED25519 = 1, // tz1
  SECP256K1 = 2, // tz2
  NISTP256r1 = 3, // tz3
  SAPLING = 4, // zet
}

interface TezosPublicKey {
  requestId: Buffer; // UUID
  publicKey: Buffer; // "edpk..."
  keyType: PublicKeyType;
  derivationPath: string | undefined; // "m/44'/1729'/0'/0'" // TODO: Change to path type?
  masterFingerprint: Buffer | undefined; // "00000000"
  label: string | undefined; // "Work Account"
}
```

#### Sign Request

> Sign a transaction or arbitrary payload

> TODO CDDL representation

```ts
// Draft

export enum DataType {
  operation = 1, // A forged operation.
}

interface SignRequest {
  requestId: Buffer; // UUID
  signData: Buffer; // "050100..." or "xyz"
  dataType: DataType;
  derivationPath?: string | undefined; // "m/44'/1729'/0'/0'"
  masterFingerprint?: Buffer | undefined; // "00000000"
}
```

- The derivation path and masterFingerprint are passed so the offline signer can re-create the account if necessary.

#### Sign Response

> Send a signature or signed transaction back to the watch only wallet

> TODO CDDL representation

```ts
// Draft
interface SignResponse {
  id: Buffer; // UUID
  signature: Buffer; // signature
  payload: Buffer | undefined; // "050100..."
}
```

- The payload is optional. If the payload is not set, the watch-only wallet needs to match the signature with the original payload using the ID.

### Handshake

No handshake between the devices is required because the communication is not encrypted. The data that is transferred is usually public and will usually be injected into the network, we decided against having the additional overhead of encryption in this protocol. However, in the future, this might be a possible addition to also allow for transmission of private data.

### Deeplinks

> TODO: This section is still under construction while we coordinate with the tzip-10 proposal.

We propose the same scheme that is used in tzip-10.

`tezos://?type=tzip25&data=<data>`

> UR specific deeplink

`UR:X-TEZOS/<data>`

## Test Cases

_TBD: Test cases for an implementation are strongly recommended as are any proofs of correctness via formal methods._

## Implementations

> Under construction

## Apendix

### Wallets implementing the standard

> Under construction - [AirGap](https://airgap.it)

### Applications using the standard

> Under construction

### Libraries working with implementations

> Under construction

## Copyright

Copyright and related rights waived via
[CC0](https://creativecommons.org/publicdomain/zero/1.0/).
